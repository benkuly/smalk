package io.terrakok.smalk.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.store.avatarUrl
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.UserId
import kotlin.math.abs

@Composable
fun Avatar(
    modifier: Modifier = Modifier,
    client: MatrixClient,
    id: String,
    avatarUrl: Flow<String?>,
    name: Flow<String?>,
    shape: RoundedCornerShape = CircleShape
) {
    var urlAndName by remember { mutableStateOf<Pair<String?, String>>(null to "") }
    LaunchedEffect(id) {
        avatarUrl
            .combine(name) { a, b -> a to b.orEmpty().trim() }
            .onEach { urlAndName = it }
            .collect()
    }
    Box(modifier) {
        val (urlStr, nameStr) = urlAndName
        val initials = nameStr.split(" ").take(2).joinToString("") { it.take(1).uppercase() }
        val txt = if (initials.length == 2) initials else nameStr.take(2).uppercase()
        Text(
            modifier = Modifier
                .fillMaxSize()
                .background(id.toColor(), shape)
                .clip(shape)
                .wrapContentHeight(),
            text = txt,
            textAlign = TextAlign.Center,
            color = Color.Black
        )
        if (urlStr != null) {
            MatrixImage(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White, shape)
                    .clip(shape),
                client,
                urlStr
            )
        } else if (nameStr.isBlank()) {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(8.dp),
                colorFilter = ColorFilter.tint(Color.Black),
                imageVector = rememberPerson(),
                contentDescription = null
            )
        }
    }
}

@Composable
fun UserAvatar(
    modifier: Modifier = Modifier,
    client: MatrixClient,
    userId: UserId,
    roomId: RoomId
) {
    val userFlow = client.user.getById(roomId, userId)
    Avatar(
        modifier,
        client,
        userId.full,
        userFlow.mapNotNull { it?.avatarUrl },
        userFlow.mapNotNull { it?.name }
    )
}

//https://hexcolor.co/palette-generator
private val avatarColors = arrayOf(
    Color(0xffefe0ff),
    Color(0xffceaeff),
    Color(0xff977fff),
    Color(0xffffe0a8),
    Color(0xffffaea5),
    Color(0xffff7fa0),
    Color(0xfff5ff98),
    Color(0xffdeff96),
    Color(0xffc3ff90),
)

private fun String.toColor(): Color {
    val code = hashCode()
    val index = abs(code) % avatarColors.size
    return avatarColors[index]
}