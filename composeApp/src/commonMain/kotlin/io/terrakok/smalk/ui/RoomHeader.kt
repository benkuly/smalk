package io.terrakok.smalk.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Badge
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.map
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.store.isEncrypted
import net.folivo.trixnity.client.store.sender
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.events.m.room.MemberEventContent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RoomHeader(
    modifier: Modifier = Modifier,
    client: MatrixClient,
    room: Room
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        val roomAvatarFlow = remember(room.roomId.full) { room.roomId.avatarFlow(client) }
        val roomNameFlow = remember(room.roomId.full) { room.roomId.nameFlow(client) }
        val roomName by roomNameFlow.collectAsState(room.roomId.full)
        Avatar(
            modifier = Modifier.padding(8.dp).size(40.dp),
            client,
            room.roomId.full,
            roomAvatarFlow,
            roomNameFlow
        )
        Column(
            modifier = Modifier.padding(end = 8.dp)
        ) {
            Row {
                Text(
                    modifier = Modifier.weight(1f),
                    text = roomName,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.labelLarge
                )
                val counter = room.unreadMessageCount
                if (counter > 0) {
                    Badge { Text(counter.toString()) }
                }
            }
            Spacer(modifier = Modifier.size(4.dp))
            LastMessageText(client, room)
        }
    }
}

@Composable
fun SpaceHeader(client: MatrixClient, room: Room) {
    Row(
        modifier = Modifier.fillMaxWidth().background(MaterialTheme.colorScheme.tertiaryContainer),
        verticalAlignment = Alignment.CenterVertically
    ) {
        val roomAvatarFlow = remember(room.roomId.full) { room.roomId.avatarFlow(client) }
        val roomNameFlow = remember(room.roomId.full) { room.roomId.nameFlow(client) }
        val roomName by roomNameFlow.collectAsState(room.roomId.full)
        Avatar(
            modifier = Modifier.padding(vertical = 8.dp, horizontal = 16.dp).size(24.dp),
            client,
            room.roomId.full,
            roomAvatarFlow,
            roomNameFlow,
            RoundedCornerShape(20)
        )
        Text(
            text = roomName,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.labelLarge
        )
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun LastMessageText(client: MatrixClient, room: Room) {
    var txt by remember { mutableStateOf("") }

    val eventId = room.lastRelevantEventId ?: room.lastEventId
    LaunchedEffect(eventId) {
        val eventFlow = if (eventId != null) {
            client.room.getTimelineEvent(room.roomId, eventId)
        } else return@LaunchedEffect

        val user = eventFlow
            .filterNotNull()
            .flatMapLatest { client.user.getById(room.roomId, it.sender) }
            .filterNotNull()
        val message = eventFlow
            .filterNotNull()
            .map { timelineEvent ->
                if (timelineEvent.isEncrypted) return@map "[encrypted]"
                when (val roomEventContent = timelineEvent.content?.getOrNull()) {
                    is RoomMessageEventContent.TextMessageEventContent -> roomEventContent.body
                    is RoomMessageEventContent.FileMessageEventContent -> "[file]"
                    is RoomMessageEventContent.ImageMessageEventContent -> "[image]"
                    is MemberEventContent -> "[${roomEventContent.membership.name}]"
                    null -> timelineEvent.event::class.simpleName
                    else -> roomEventContent::class.simpleName
                }
            }
        user.flatMapMerge { u -> message.map { "${u.name}: $it" } }
            .collect { txt = it }
    }
    Text(
        text = txt,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        style = MaterialTheme.typography.bodySmall
    )
}