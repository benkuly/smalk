package io.terrakok.smalk.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import io.github.aakira.napier.Napier
import io.terrakok.smalk.LocalPhoneMode
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.TimelineState
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.EventId
import net.folivo.trixnity.core.model.RoomId
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class RoomScreen(
    val roomId: RoomId?,
    private val client: MatrixClient
) : Screen {
    override val key = uniqueScreenKey

    companion object : KoinComponent {
        fun create(roomId: RoomId?) = RoomScreen(roomId, get())
    }

    @Composable
    override fun Content() {
        if (roomId == null) {
            val phoneMode by LocalPhoneMode.current
            if (!phoneMode) {
                Text(
                    modifier = Modifier.fillMaxSize().wrapContentHeight(),
                    text = "Select chat",
                    textAlign = TextAlign.Center
                )
            }
        } else {
            Scaffold(
                topBar = {
                    Surface(shadowElevation = 3.dp) {
                        RoomTopBar(roomId)
                    }
                },
                content = {
                    Timeline(roomId, it)
                }
            )
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    private fun RoomTopBar(id: RoomId) {
        val navigator = LocalNavigator.currentOrThrow
        TopAppBar(
            title = {
                val name by id.nameFlow(client).collectAsState(id.full)
                Text(
                    text = name,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            },
            navigationIcon = {
                val phoneMode by LocalPhoneMode.current
                if (phoneMode) {
                    IconButton(onClick = { navigator.replaceAll(RoomScreen(null, client)) }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = null)
                    }
                }
            }
        )
    }

    @Composable
    private fun Timeline(
        roomId: RoomId,
        paddingValues: PaddingValues
    ) {
        data class TimelineItem(val eventId: EventId, val timelineEventFlow: Flow<TimelineEvent>)

        val scope = rememberCoroutineScope()
        val timeline = remember(roomId) { client.room.getTimeline(roomId) { TimelineItem(it.first().eventId, it) } }
        val timelineState by timeline.state.collectAsState(TimelineState())

        LaunchedEffect(timeline) {
            Napier.d { "init timeline $roomId" }
            val room = client.room.getById(roomId).filterNotNull()
            val initTimelineFrom = room.mapNotNull { it.lastEventId }.first()
            timeline.init(initTimelineFrom)
            if (!room.first().membersLoaded) {
                client.user.loadMembers(roomId, false)
            }
        }

        LazyColumn(
            modifier = Modifier.fillMaxSize().padding(horizontal = 8.dp),
            contentPadding = paddingValues,
            reverseLayout = true,
        ) {
            if (timelineState.isLoadingAfter) {
                item {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        CircularProgressIndicator(
                            modifier = Modifier.padding(8.dp).align(Alignment.Center).size(16.dp)
                        )
                    }
                }
            }
            itemsIndexed(
                timelineState.elements.reversed(),
                key = { _, element -> element.eventId.full }
            ) { index, (_, event) ->
                if (index > timelineState.elements.size - 5 && !timelineState.isLoadingBefore && timelineState.canLoadBefore) {
                    scope.launch {
                        Napier.d("loadBefore [${roomId.full}]")
                        timeline.loadBefore()
                    }
                }
                Spacer(modifier = Modifier.size(8.dp))
                TimelineElement(client, event)
            }
            item {
                if (timelineState.isLoadingBefore) {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        CircularProgressIndicator(
                            modifier = Modifier.padding(8.dp).align(Alignment.Center).size(16.dp)
                        )
                    }
                } else {
                    Spacer(modifier = Modifier.size(8.dp))
                }
            }
        }
    }
}