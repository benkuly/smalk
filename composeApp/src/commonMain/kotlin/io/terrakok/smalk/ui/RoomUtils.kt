package io.terrakok.smalk.ui

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.runningFold
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.RoomService
import net.folivo.trixnity.client.room.getAllState
import net.folivo.trixnity.client.room.getState
import net.folivo.trixnity.client.store.avatarUrl
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.AvatarEventContent
import net.folivo.trixnity.core.model.events.m.room.CreateEventContent
import net.folivo.trixnity.core.model.events.m.room.NameEventContent
import net.folivo.trixnity.core.model.events.m.space.ChildEventContent
import net.folivo.trixnity.core.model.events.m.space.ParentEventContent

suspend fun RoomService.getSubTree(rootId: RoomId): List<RoomId> {
    val result = mutableListOf<RoomId>()
    getChildren(rootId).forEach { id ->
        result.add(id)
        result.addAll(getSubTree(id))
    }
    return result
}

suspend fun RoomService.isSpace(roomId: RoomId) =
    getState<CreateEventContent>(roomId).first()?.content?.type == CreateEventContent.RoomType.Space

suspend fun RoomService.isRoot(roomId: RoomId) =
    getAllState<ParentEventContent>(roomId)
        .first()
        .isNullOrEmpty()

suspend fun RoomService.getChildren(roomId: RoomId) =
    getAllState<ChildEventContent>(roomId)
        .first()
        .orEmpty()
        .keys
        .map { RoomId(it) }

fun RoomId.avatarFlow(client: MatrixClient): Flow<String?> {
    return client.room.getState<AvatarEventContent>(this).flatMapLatest { event ->
        event?.content?.url?.let { return@flatMapLatest flowOf(it) }

        client.room.getById(this).flatMapLatest room@{ room ->
            room?.avatarUrl?.let { return@room flowOf(it) }
            val singleHero = room?.name?.heroes?.singleOrNull() ?: return@room flowOf(null)
            client.user.getById(room.roomId, singleHero).map { it?.avatarUrl }
        }
    }
}

fun RoomId.nameFlow(client: MatrixClient): Flow<String> {
    return client.room.getState<NameEventContent>(this).flatMapLatest { event ->
        event?.content?.name?.let { return@flatMapLatest flowOf(it) }

        client.room.getById(this).flatMapLatest room@{ room ->
            val name = room?.name ?: return@room flowOf(this.full)
            name.explicitName?.let { return@room flowOf(it) }
            name.heroes.map { userId -> client.user.getById(room.roomId, userId).filterNotNull() }
                .asFlow()
                .flattenMerge()
                .runningFold(mutableMapOf<String, String>()) { acc, value ->
                    acc[value.userId.full] = value.name
                    acc
                }
                .map { acc -> acc.values.joinToString() }
        }
    }
}