package io.terrakok.smalk.ui

import Smalk.composeApp.BuildConfig
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import io.terrakok.smalk.service.SessionManager
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class LoginScreen(
    private val sessionManager: SessionManager
) : Screen {
    override val key = uniqueScreenKey

    companion object : KoinComponent {
        fun create() = LoginScreen(get())
    }

    @Composable
    override fun Content() {
        val scope = rememberCoroutineScope()
        val navigator = LocalNavigator.currentOrThrow
        var server by remember { mutableStateOf("matrix.org") }
        var username by remember { mutableStateOf(BuildConfig.DEFAULT_USER) }
        var password by remember { mutableStateOf(BuildConfig.DEFAULT_PWD) }

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            OutlinedCard(
                modifier = Modifier.width(400.dp).padding(24.dp)
            ) {
                Column(
                    modifier = Modifier.padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        value = server,
                        onValueChange = { server = it },
                        label = { Text("Homeserver") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    OutlinedTextField(
                        value = username,
                        onValueChange = { username = it },
                        label = { Text("Username") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    PasswordTextField(
                        password = password,
                        onPasswordChange = { password = it },
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.size(16.dp))

                    var isProgress by remember { mutableStateOf(false) }
                    val msgDialogState = remember { mutableStateOf(false) }
                    var message by remember { mutableStateOf("") }
                    MessageDialog(message, msgDialogState)

                    ProgressButton(
                        modifier = Modifier.align(Alignment.End),
                        enabled = server.isNotBlank() && username.isNotBlank() && password.isNotBlank(),
                        progress = isProgress,
                        text = "Login",
                        onClick = {
                            scope.launch {
                                isProgress = true
                                try {
                                    sessionManager.login(server, username, password)
                                    navigator.replace(HomeScreen.create())
                                } catch (e: Exception) {
                                    message = e.message.orEmpty()
                                    msgDialogState.value = true
                                }
                                isProgress = false
                            }
                        }
                    )
                }
            }
        }
    }

    @Composable
    private fun PasswordTextField(
        password: String,
        onPasswordChange: (String) -> Unit,
        modifier: Modifier = Modifier,
        label: String = "Password"
    ) {
        var passwordVisibility by remember { mutableStateOf(false) }

        OutlinedTextField(
            value = password,
            onValueChange = onPasswordChange,
            modifier = modifier,
            maxLines = 1,
            singleLine = true,
            label = { Text(label) },
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password
            ),
            trailingIcon = {
                IconButton(
                    modifier = Modifier.size(24.dp),
                    onClick = { passwordVisibility = !passwordVisibility }
                ) {
                    val imageVector = if (passwordVisibility) rememberVisibility() else rememberVisibilityOff()
                    Icon(imageVector, contentDescription = if (passwordVisibility) "Hide password" else "Show password")
                }
            }
        )
    }

    @Composable
    private fun ProgressButton(
        modifier: Modifier,
        text: String,
        progress: Boolean,
        enabled: Boolean,
        onClick: () -> Unit
    ) {
        Button(
            modifier = modifier,
            onClick = onClick,
            enabled = enabled && !progress
        ) {
            if (progress) {
                CircularProgressIndicator(
                    modifier = Modifier.size(16.dp),
                    strokeWidth = 1.dp
                )
                Spacer(Modifier.size(ButtonDefaults.IconSpacing))
            }
            Text(text)
        }
    }

    @Composable
    private fun MessageDialog(
        message: String,
        state: MutableState<Boolean>
    ) {
        if (state.value) {
            AlertDialog(
                onDismissRequest = {
                    state.value = false
                },
                text = {
                    Text(message)
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            state.value = false
                        }
                    ) {
                        Text("OK")
                    }
                }
            )
        }
    }
}
