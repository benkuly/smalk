package io.terrakok.smalk.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import io.terrakok.smalk.LocalAppScope
import io.terrakok.smalk.LocalPhoneMode
import io.terrakok.smalk.service.SessionManager
import io.terrakok.smalk.theme.LocalThemeIsDark
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.flatten
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.clientserverapi.client.SyncState
import net.folivo.trixnity.core.model.RoomId
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class HomeScreen(
    private val sessionManager: SessionManager
) : Screen {
    override val key = uniqueScreenKey
    private val client = sessionManager.getClient()

    companion object : KoinComponent {
        fun create() = HomeScreen(get())
    }

    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow

        LaunchedEffect(Unit) {
            client.startSync()
            client.loginState.onEach {
                if (it != MatrixClient.LoginState.LOGGED_IN) {
                    navigator.replaceAll(LoginScreen.create())
                }
            }.collect()
        }

        var selectedRoomId: RoomId? by remember { mutableStateOf(null) }
        val homeView = remember {
            movableContentOf {
                HomeView(selectedRoomId = selectedRoomId, onRoomClick = { selectedRoomId = it })
            }
        }
        val roomContainer = remember(selectedRoomId) {
            movableContentOf {
                Navigator(RoomScreen.create(selectedRoomId)) {
                    val currentScreen = it.lastItem
                    if (currentScreen is RoomScreen) {
                        selectedRoomId = currentScreen.roomId
                    }
                    CurrentScreen()
                }
            }
        }
        val phoneMode by LocalPhoneMode.current
        if (phoneMode) {
            Box(modifier = Modifier.fillMaxSize()) {
                homeView()
                roomContainer()
            }
        } else {
            Row(modifier = Modifier.fillMaxSize()) {
                Box(modifier = Modifier.fillMaxHeight().width(350.dp)) {
                    homeView()
                }
                Divider(modifier = Modifier.fillMaxHeight().width(DividerDefaults.Thickness))
                Box(modifier = Modifier.fillMaxHeight().weight(1f)) {
                    roomContainer()
                }
            }
        }
    }

    @Composable
    private fun HomeView(
        selectedRoomId: RoomId?,
        onRoomClick: (RoomId) -> Unit
    ) {
        var showChatList by remember { mutableStateOf(true) }
        Scaffold(
            topBar = {
                Surface(shadowElevation = 3.dp) {
                    HomeTopBar()
                }
            },
            content = { innerPadding ->
                if (showChatList) {
                    RoomList(innerPadding, selectedRoomId, onRoomClick)
                } else {
                    SpaceList(innerPadding, selectedRoomId, onRoomClick)
                }
            },
            bottomBar = {
                SmallNavigationBar {
                    NavigationBarItem(
                        selected = showChatList,
                        onClick = { showChatList = true },
                        icon = {
                            Icon(
                                modifier = Modifier.size(24.dp),
                                imageVector = rememberChat(),
                                contentDescription = null
                            )
                        },
                        label = null
                    )
                    NavigationBarItem(
                        selected = !showChatList,
                        onClick = { showChatList = false },
                        icon = {
                            Icon(
                                modifier = Modifier.size(24.dp),
                                imageVector = rememberFolderCopy(),
                                contentDescription = null
                            )
                        },
                        label = null
                    )
                }
            }
        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    private fun HomeTopBar() {
        val navigator = LocalNavigator.currentOrThrow
        val appScope = LocalAppScope.current
        CenterAlignedTopAppBar(
            title = {
                val syncState by client.syncState.collectAsState()
                val stateName = when (syncState) {
                    SyncState.INITIAL_SYNC -> "Connecting..."
                    SyncState.STARTED, SyncState.RUNNING, SyncState.TIMEOUT -> "Smalk"
                    SyncState.ERROR -> "No connection..."
                    SyncState.STOPPING -> "Stopping"
                    SyncState.STOPPED -> "Stopped"
                }
                Text(
                    stateName,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            },
            navigationIcon = {
                var showLogoutDialog by remember { mutableStateOf(false) }
                if (showLogoutDialog) {
                    AlertDialog(
                        onDismissRequest = { showLogoutDialog = false },
                        text = { Text("Do you want to logout?") },
                        confirmButton = {
                            Button(onClick = {
                                showLogoutDialog = false
                                appScope.launch {
                                    delay(50) //TODO fix racing on state text
                                    sessionManager.logout()
                                }
                                navigator.replaceAll(LoginScreen.create())
                            }) {
                                Text("Logout")
                            }
                        },
                        dismissButton = {
                            Button(onClick = {
                                showLogoutDialog = false
                            }) {
                                Text("Cancel")
                            }
                        },
                    )
                }
                IconButton(onClick = { showLogoutDialog = true }) {
                    Avatar(
                        client = client,
                        id = client.userId.full,
                        avatarUrl = client.avatarUrl,
                        name = client.displayName,
                    )
                }
            },
            actions = {
                var isDark by LocalThemeIsDark.current
                IconButton(onClick = { isDark = !isDark }) {
                    Icon(
                        modifier = Modifier.padding(8.dp),
                        imageVector = if (isDark) rememberLightMode() else rememberDarkMode(),
                        contentDescription = null
                    )
                }
            }
        )
    }

    @Composable
    private fun RoomList(
        paddingValues: PaddingValues,
        selectedRoomId: RoomId?,
        onRoomClick: (RoomId) -> Unit
    ) {
        var rooms by remember { mutableStateOf<List<Room>>(emptyList()) }
        LaunchedEffect(Unit) {
            val roomService = client.room
            roomService.getAll()
                .flatten()
                .map { allRooms ->
                    allRooms.filter { !roomService.isSpace(it.roomId) && roomService.isRoot(it.roomId) }
                }
                .onEach { allRooms ->
                    rooms = allRooms.sortedByDescending { it.lastRelevantEventTimestamp?.epochSeconds }
                }
                .collect()
        }

        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = paddingValues
        ) {
            items(rooms) {
                val isSelected = remember(selectedRoomId) { selectedRoomId == it.roomId }
                RoomHeader(
                    modifier = Modifier
                        .clickable { onRoomClick(it.roomId) }
                        .background(
                            if (isSelected) MaterialTheme.colorScheme.primaryContainer else MaterialTheme.colorScheme.surface
                        ),
                    client,
                    it
                )
            }
        }
    }

    @Composable
    private fun SpaceList(
        paddingValues: PaddingValues,
        selectedRoomId: RoomId?,
        onRoomClick: (RoomId) -> Unit
    ) {
        data class RoomIsSpace(val room: Room, val isSpace: Boolean)

        var rooms by remember { mutableStateOf<List<RoomIsSpace>>(emptyList()) }

        LaunchedEffect(Unit) {
            val roomService = client.room
            roomService.getAll()
                .flatten()
                .map { allRooms -> allRooms.associateBy { it.roomId } }
                .onEach { allRooms ->
                    val newRooms = mutableListOf<RoomIsSpace>()
                    val roots = allRooms.values.filter {
                        roomService.isSpace(it.roomId) && roomService.isRoot(it.roomId)
                    }
                    roots.forEach { root ->
                        newRooms.add(RoomIsSpace(root, true))
                        newRooms.addAll(
                            roomService.getSubTree(root.roomId).mapNotNull { id ->
                                allRooms[id]?.let { room -> RoomIsSpace(room, roomService.isSpace(id)) }
                            }
                        )
                    }
                    rooms = newRooms
                }
                .collect()
        }

        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = paddingValues
        ) {
            items(rooms) { (room, isSpace) ->
                val isSelected = remember(selectedRoomId) { selectedRoomId == room.roomId }
                if (isSpace) {
                    SpaceHeader(client, room)
                } else {
                    RoomHeader(
                        modifier = Modifier
                            .clickable { onRoomClick(room.roomId) }
                            .background(
                                if (isSelected) MaterialTheme.colorScheme.primaryContainer else MaterialTheme.colorScheme.surface
                            ),
                        client,
                        room
                    )
                }
            }
        }
    }
}