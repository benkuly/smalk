package io.terrakok.smalk.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.Flow
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.core.model.events.m.room.MemberEventContent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent

@Composable
fun TimelineElement(client: MatrixClient, element: Flow<TimelineEvent>) {
    val itemState by element.collectAsState(null)
    val item = itemState
    if (item != null) {
        val itemText = when (val content = item.content?.getOrNull()) {
            is RoomMessageEventContent.TextMessageEventContent -> content.body
            is MemberEventContent -> content.membership.value
            else -> "[${content?.let { it::class.simpleName }}]" //TODO
        }

        val isMy = item.event.sender == client.userId
        val shape = remember(isMy) {
            RoundedCornerShape(
                topStart = 8.dp,
                topEnd = 8.dp,
                bottomStart = if (isMy) 8.dp else 0.dp,
                bottomEnd = if (!isMy) 8.dp else 0.dp
            )
        }
        val padding = remember(isMy) {
            PaddingValues(
                start = if (isMy) 40.dp else 0.dp,
                top = 0.dp,
                end = if (isMy) 0.dp else 40.dp,
                bottom = 0.dp
            )
        }
        val bubbleBg = if (isMy) {
            MaterialTheme.colorScheme.tertiaryContainer
        } else {
            MaterialTheme.colorScheme.secondaryContainer
        }
        val bubbleTextColor = if (isMy) {
            MaterialTheme.colorScheme.onTertiaryContainer
        } else {
            MaterialTheme.colorScheme.onSecondaryContainer
        }
        Row {
            if (!isMy) {
                UserAvatar(
                    modifier = Modifier.align(Alignment.Bottom).padding(end = 8.dp).size(32.dp),
                    client,
                    item.event.sender,
                    item.roomId
                )
            } else {
                Spacer(modifier = Modifier.weight(1f))
            }
            Text(
                modifier = Modifier
                    .padding(padding)
                    .widthIn(min = 100.dp)
                    .clip(shape)
                    .background(bubbleBg)
                    .padding(16.dp),
                text = itemText,
                color = bubbleTextColor,
                style = MaterialTheme.typography.bodySmall
            )
            if (isMy) {
                UserAvatar(
                    modifier = Modifier.align(Alignment.Bottom).padding(start = 8.dp).size(32.dp),
                    client,
                    item.event.sender,
                    item.roomId
                )
            }
        }
    } else {
        Spacer(modifier = Modifier.size(40.dp))
    }
}