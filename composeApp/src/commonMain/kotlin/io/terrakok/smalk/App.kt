package io.terrakok.smalk

import Smalk.composeApp.BuildConfig
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.Navigator
import io.github.aakira.napier.Napier
import io.terrakok.smalk.service.SessionManager
import io.terrakok.smalk.service.getLogger
import io.terrakok.smalk.service.getPlatformSettings
import io.terrakok.smalk.theme.AppTheme
import io.terrakok.smalk.ui.HomeScreen
import io.terrakok.smalk.ui.LoginScreen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.mp.KoinPlatform

internal val LocalAppScope = compositionLocalOf<CoroutineScope> { GlobalScope }
internal val LocalPhoneMode = compositionLocalOf { mutableStateOf(false) }

@Composable
internal fun App() {
    initApp()

    Napier.d("Start application")
    val sessionManager: SessionManager by remember { KoinPlatform.getKoin().inject() }

    DisposableEffect(Unit) {
        onDispose {
            Napier.d("Stop application")
            sessionManager.stop()
        }
    }

    CompositionLocalProvider(
        LocalAppScope provides rememberCoroutineScope()
    ) {
        var isPhone by LocalPhoneMode.current
        val density = LocalDensity.current
        Box(Modifier.fillMaxSize().onGloballyPositioned {
            with(density) {
                isPhone = it.size.width.toDp() <= 700.dp
            }
        }) {
            AppTheme {
                var launchScreen by remember { mutableStateOf<Screen?>(null) }
                LaunchedEffect(Unit) {
                    val hasPreviousSession = sessionManager.tryRestoreSession()
                    launchScreen = if (hasPreviousSession) {
                        HomeScreen.create()
                    } else {
                        LoginScreen.create()
                    }
                }
                launchScreen?.let { Navigator(it) }
            }
        }
    }
}

private var isInit = false
private fun initApp() {
    if (isInit) {
        Napier.e("Second initialization!")
        return
    }
    isInit = true

    if (BuildConfig.DEBUG) {
        Napier.base(getLogger("Smalk"))
    }

    val appModule = module {
        single { getPlatformSettings() }
        single { SessionManager(get()) }
        factory { get<SessionManager>().getClient() }
    }
    startKoin {
        modules(appModule)
    }
}
