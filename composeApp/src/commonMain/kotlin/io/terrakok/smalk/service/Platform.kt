package io.terrakok.smalk.service

import androidx.compose.ui.graphics.ImageBitmap
import com.russhwolf.settings.Settings
import io.github.aakira.napier.DebugAntilog
import okio.Path

expect fun getPlatformSettings(): Settings
expect fun getCacheDirectoryPath(): Path
expect fun ByteArray.asImageBitmap(): ImageBitmap
expect fun getLogger(defaultTag: String): DebugAntilog