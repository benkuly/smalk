package io.terrakok.smalk.service

import com.russhwolf.settings.Settings
import com.russhwolf.settings.nullableString
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.MatrixClientConfiguration
import net.folivo.trixnity.client.fromStore
import net.folivo.trixnity.client.login
import net.folivo.trixnity.client.media.okio.OkioMediaStore
import net.folivo.trixnity.client.serverDiscovery
import net.folivo.trixnity.client.store.repository.realm.createRealmRepositoriesModule
import net.folivo.trixnity.clientserverapi.model.authentication.IdentifierType
import net.folivo.trixnity.core.model.UserId

class SessionManager(settings: Settings) {
    private var client: MatrixClient? = null
    private var deviceId by settings.nullableString("DEVICE_ID")

    private val mediaStore by lazy { createMediaStore() }
    private val repositoriesModule by lazy { createRepositoriesModule() }

    suspend fun tryRestoreSession(): Boolean {
        Napier.d("Try restore session [$deviceId]")
        val restored = MatrixClient.fromStore(
            mediaStore = mediaStore,
            repositoriesModule = repositoriesModule,
            configuration = clientConfig
        ).getOrNull()

        if (restored != null) {
            client = restored
            return true
        } else {
            return false
        }
    }

    suspend fun login(server: String, username: String, password: String) {
        Napier.d("Open session [$deviceId]")
        val userId = UserId(username, server)
        val url = userId.serverDiscovery().getOrThrow()
        client = MatrixClient.login(
            baseUrl = url,
            mediaStore = mediaStore,
            repositoriesModule = repositoriesModule,
            identifier = IdentifierType.User(username),
            password = password,
            deviceId = deviceId,
            configuration = clientConfig
        ).getOrThrow()
        deviceId = client?.deviceId
    }

    suspend fun logout() {
        Napier.d("End session")
        client?.apply {
            logout()
            clearCache()
            clearMediaCache()
            stop()
        }
        client = null
    }

    fun stop() {
        Napier.d("Stop session")
        client?.stop()
        client = null
    }

    fun getClient() = client ?: error("Session client is NULL!")

    private fun createMediaStore() = OkioMediaStore(
        getCacheDirectoryPath().resolve("media")
    )

    private fun createRepositoriesModule() = createRealmRepositoriesModule {
        directory(getCacheDirectoryPath().resolve("realm").toString())
    }

    private val clientConfig: MatrixClientConfiguration.() -> Unit = {
        httpClientFactory = {
            HttpClient {
                it()
                install(Logging) {
                    level = LogLevel.ALL
                    logger = object : Logger {
                        override fun log(message: String) {
                            Napier.d(tag = "HTTP Client", message = message)
                        }
                    }
                }
            }
        }
    }
}