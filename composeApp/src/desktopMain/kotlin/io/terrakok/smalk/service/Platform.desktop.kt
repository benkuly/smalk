package io.terrakok.smalk.service

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.russhwolf.settings.PreferencesSettings
import com.russhwolf.settings.Settings
import io.github.aakira.napier.DebugAntilog
import okio.Path
import okio.Path.Companion.toPath
import org.jetbrains.skia.Image
import java.util.logging.ConsoleHandler
import java.util.logging.Formatter
import java.util.logging.Level
import java.util.logging.LogRecord
import java.util.prefs.Preferences


private object SmalkPreferences

actual fun getPlatformSettings(): Settings = PreferencesSettings(
    Preferences.userNodeForPackage(SmalkPreferences::class.java)
)

actual fun getCacheDirectoryPath(): Path =
    System.getProperty("user.home").toPath().resolve(".Smalk/cache")

actual fun ByteArray.asImageBitmap(): ImageBitmap =
    Image.makeFromEncoded(this).toComposeImageBitmap()

private val logHandler = object : ConsoleHandler() {
    init {
        setOutputStream(System.out)
        level = Level.ALL
        formatter = object : Formatter() {
            override fun format(record: LogRecord) = "🔥 ${record.message}\n"
        }
    }
}
actual fun getLogger(defaultTag: String): DebugAntilog =
    DebugAntilog(defaultTag, listOf(logHandler))