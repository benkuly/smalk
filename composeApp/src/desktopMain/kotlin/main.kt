import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import io.terrakok.smalk.App
import java.awt.Dimension

fun main() = application {
    Window(
        title = "Smalk",
        state = rememberWindowState(width = 1000.dp, height = 700.dp),
        onCloseRequest = ::exitApplication,
    ) {
        window.minimumSize = Dimension(350, 600)
        App()
    }
}