import androidx.compose.ui.window.ComposeUIViewController
import io.terrakok.smalk.App
import platform.UIKit.UIViewController

fun MainViewController(): UIViewController {
    return ComposeUIViewController { App() }
}
